package com.prueba.elkin.pruebaelkinsepulveda;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prueba.elkin.pruebaelkinsepulveda.Services.ImeiService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    static final String URLSERVER= "url";
    static final String SERVICETYPE= "servicetype";
    static final String DATA = "contenido";

    ProgressDialog progressDialog;
    EditText Url, Data;
    Spinner spinner;

    final int PERMISSION_REQUEST_CODE = 111;

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);

        spinner = findViewById(R.id.spinner);
        Url = findViewById(R.id.textUrl);
        Data = findViewById(R.id.textData);

        //Se cargan los datos guardados en las preferencias (Si existen)
        sharedPref.getInt(SERVICETYPE,0);
        Url.setText(sharedPref.getString(URLSERVER,""));
        Data.setText(sharedPref.getString(DATA,""));
        //

        //Se crea la lista del spinner
        List<String> categories = new ArrayList<String>();
        categories.add("get");
        categories.add("post");
        categories.add("put");
        categories.add("delete");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        //Verificacion y solicitud de permisos para versiones 23 y/o superior
        if (Build.VERSION.SDK_INT >= 23) {
            if (!fineLocationPermission() ||
                    !coarseLocationPermission() ||
                    !readPhoneStatePermission()) {
                requestPermission();
            } else {
                startApp();
            }
        } else {
            startApp();
        }
    }
    //metodo para ejecutar la peticion
    public void download(View view) {
        if (!Url.getText().toString().equals("") && !Data.getText().toString().equals("")) {

            switch (spinner.getSelectedItemPosition()) {
                case 0:
                    getRequest();
                    break;
                case 1:
                    serverRequest(Request.Method.POST);
                    break;
                case 2:
                    serverRequest(Request.Method.PUT);
                    break;
                case 3:
                    serverRequest(Request.Method.DELETE);
                    break;
            }
        } else {
            Toast.makeText(MainActivity.this, "Por favor llene los campos", Toast.LENGTH_SHORT).show();
        }
    }


    public void getRequest() {
        progressDialog = ProgressDialog.show(MainActivity.this, "Atencion!", "Cargando...");
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Url.getText().toString() + Data.getText().toString();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.i("*Response", response);

                        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Atencion");
                        builder.setMessage(response.toString());
                        builder.setCancelable(false);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getApplicationContext(),"saliendo",Toast.LENGTH_SHORT).show();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        Toast.makeText(MainActivity.this, "respuesta" + response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();
                Log.i("*Error", error.toString());
            }
        });
        queue.add(stringRequest);

    }
    //post,put,delete
    public void serverRequest(int method) {
        progressDialog = ProgressDialog.show(MainActivity.this, "Atencion!", "Cargando...");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(Data.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Url.getText().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                method,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Atencion");
                        builder.setMessage(response.toString());
                        builder.setCancelable(false);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getApplicationContext(),"saliendo",Toast.LENGTH_SHORT).show();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        Toast.makeText(MainActivity.this, "respuesta" + response, Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                        progressDialog.dismiss();
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }

    public void goToMap(View view) {
        //Se guardanlos datos en las preferencias para ser cargados posteriormente
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(URLSERVER, Url.getText().toString());
        editor.putInt(SERVICETYPE, spinner.getSelectedItemPosition());
        editor.putString(DATA, Data.getText().toString());
        editor.commit();
        //
        Intent i = new Intent(MainActivity.this, MapsActivity.class);
        startActivity(i);
        finish();
    }

    private void startApp() {
        if(!isMyServiceRunning(ImeiService.class)) {
            startService(new Intent(getApplicationContext(), ImeiService.class));
        }
    }
    //metodos de verificacion de los permisos
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean fineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean coarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean readPhoneStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    //


    //Solicitud de los permisos
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startApp();
                } else {
                    finish();
                }
                break;
        }
    }
}


